import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Задача 1
        System.out.print("Введите строку: ");
        String input = scanner.nextLine();
        String[] words = input.split("\\s+");
        boolean replace = false;
        for (int i = 0; i < words.length; i++) {
            String word = words[i].toLowerCase();
            if (word.equals("object-oriented") && replace) {
                words[i] = "OOP";
                replace = false;
            } else if (word.equals("programming")) {
                replace = !replace;
            }
        }
        String output = String.join(" ", words);
        System.out.println(output);

        // Задача 2
        System.out.print("Введите строку: ");
        input = scanner.nextLine();
        words = input.split("\\s+");
        String minWord = words[0];
        int minUnique = countUnique(minWord);
        for (int i = 1; i < words.length; i++) {
            String word = words[i];
            int unique = countUnique(word);
            if (unique < minUnique) {
                minUnique = unique;
                minWord = word;
            }
        }
        System.out.println("Слово с минимальным количеством уникальных символов: " + minWord);

        // Задача 3
        System.out.print("Введите строку: ");
        input = scanner.nextLine();
        words = input.split("\\s+");
        int latinWords = 0;
        for (String word : words) {
            if (word.matches("[a-zA-Z]+")) {
                latinWords++;
            }
        }
        System.out.println("Количество слов, содержащих только символы латинского алфавита: " + latinWords);

        // Задача 4
        System.out.print("Введите строку: ");
        input = scanner.nextLine();
        words = input.split("\\s+");
        System.out.print("Палиндромы: ");
        for (String word : words) {
            if (isPalindrome(word)) {
                System.out.print(word + " ");
            }
        }
    }

    private static int countUnique(String word) {
        int unique = 0;
        boolean[] seen = new boolean[256];
        for (int i = 0; i < word.length(); i++) {
            int ch = word.charAt(i);
            if (!seen[ch]) {
                unique++;
                seen[ch] = true;
            }
        }
        return unique;
    }

    private static boolean isPalindrome(String word) {
        int left = 0;
        int right = word.length() - 1;
        while (left < right) {
            if (word.charAt(left) != word.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

}